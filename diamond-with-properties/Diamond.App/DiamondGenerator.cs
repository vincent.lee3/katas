﻿using System.Collections.Generic;

namespace Diamond.App
{
    public static class DiamondGenerator
    {
        private const char PaddingChar = '-';
        private const char StartChar = 'A';

        public static IEnumerable<string> Generate(char c)
        {


            yield return StartChar.ToString();
            yield return $"{c}{PaddingChar}{c}";
            yield return StartChar.ToString();
        }
    }
}