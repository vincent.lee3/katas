﻿using FsCheck;

namespace Diamond.App.Tests
{
    public static class LetterGenerator
    {
        public static Arbitrary<char> Generate() =>
            Arb.Default.Char()
                .Filter(c => c >= 'A' && c <= 'Z');
    }
    public static class LetterNotAGenerator
    {
        public static Arbitrary<char> Generate() =>
            Arb.Default.Char()
                .Filter(c => c >= 'B' && c <= 'Z');
    }
}