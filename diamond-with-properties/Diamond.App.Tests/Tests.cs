﻿using System;
using System.Collections.Immutable;
using System.Linq;
using FsCheck;
using FsCheck.Xunit;

namespace Diamond.App.Tests
{
    public class Tests
    {
        [Property(Arbitrary = new[] {typeof(LetterGenerator)})]
        public Property NotEmpty(char c)
        {
            return DiamondGenerator.Generate(c)
                .All(s => s != string.Empty)
                .ToProperty();
        }

        [Property(Arbitrary = new[] {typeof(LetterGenerator)})]
        public Property FirstLineAlwaysContainsSingleA(char c)
        {
            return DiamondGenerator.Generate(c)
                .First()
                .Contains("A")
                .ToProperty();
        }

        [Property(Arbitrary = new[] {typeof(LetterGenerator)})]
        public Property LastLineAlwaysContainsSingleA(char c)
        {
            return DiamondGenerator.Generate(c)
                .Last()
                .Contains("A")
                .ToProperty();
        }

        [Property(Arbitrary = new[] {typeof(LetterGenerator)})]
        public Property FirstCharOfMiddleLineAlwaysEqualsTheInput(char c)
        {
            var result = DiamondGenerator.Generate(c).ToImmutableArray();
            var middleIndex = (int)Math.Ceiling(result.Count() / 2.0) - 1;
            return (result
                    .ElementAt(middleIndex)
                    .First() == c)
                .ToProperty();
        }

        [Property(Arbitrary = new[] {typeof(LetterGenerator)})]
        public Property LastCharOfMiddleLineAlwaysEqualsTheInput(char c)
        {
            var result = DiamondGenerator.Generate(c).ToImmutableArray();
            var middleIndex = (int)Math.Ceiling(result.Count() / 2.0) - 1;
            return (result
                    .ElementAt(middleIndex)
                    .Last() == c)
                .ToProperty();
        }

        [Property(Arbitrary = new[] {typeof(LetterNotAGenerator)})]
        public Property LinesNotFirstAndLastContainsInternalWhitespace(char c)
        {
            var result = DiamondGenerator.Generate(c).ToImmutableArray();
            var notFirstAndLastLines = result
                .Skip(1)
                .Take(result.Length - 2);

            return notFirstAndLastLines
                .All(l => l.LastIndexOf(c) - l.IndexOf(c) > 1)
                .ToProperty();
        }

        [Property(Arbitrary = new[] {typeof(LetterGenerator)})]
        public Property AllLinesArePalindromic(char c)
        {
            var result = DiamondGenerator.Generate(c).ToImmutableArray();

            return result
                .All(l => l.Reverse().SequenceEqual(l))
                .ToProperty();
        }

        // [Property(Arbitrary = new[] {typeof(LetterNotAGenerator)})]
        // public Property InnerPaddingIsCorrect(char c)
        // {
        //     var result = DiamondGenerator.Generate(c).ToImmutableArray();
        //     var notFirstAndLastLines = result
        //         .Skip(1)
        //         .Take(result.Length - 2);
        //
        //     return notFirstAndLastLines
        //         .All(l =>
        //         {
        //             var n = c - 'A' + 1;
        //             var expectedPadding = 2 * (n - 1) - 1;
        //             return l.LastIndexOf(c) - l.IndexOf(c) - 1 == expectedPadding;
        //         })
        //         .ToProperty();
        // }

        // [Property(Arbitrary = new[] {typeof(LetterGenerator)})]
        // public Property TheHeightIsAlways2NMinus1(char c)
        // {
        //     var result = DiamondGenerator.Generate(c).ToImmutableArray();
        //     var n = c - 'A' + 1;
        //     var expectedHeight = 2 * n - 1;
        //     return (result.Length == expectedHeight).ToProperty();
        // }
    }
}